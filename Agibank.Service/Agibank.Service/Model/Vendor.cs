﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Agibank.Service.Model
{
    public class Vendor
    {
        public Vendor(string cPF, string name, decimal salary)
        {
            CPF = cPF;
            Name = name;
            Salary = salary;
            this.Sales = new List<Sale>();
        }

        public string CPF { get; set; }
        public string Name { get; set; }
        public decimal Salary { get; set; }
        public List<Sale> Sales { get; set; }

        public (decimal Value, int IdSaleMajor) SaleMajor
        {
            get
            {
                var ret = Sales.OrderByDescending(x => x.ValueSum).FirstOrDefault();
                return (ret.ValueSum, ret.SaleId);
            }
        }
        public decimal SaleMinor
        {
            get
            {
                var ret = Sales.OrderByDescending(x => x.ValueSum).FirstOrDefault();
                return (ret.ValueSum);
            }
        }
    }
}
