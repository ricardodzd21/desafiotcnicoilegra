﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agibank.Service.Model
{
    //003çSale IDç[Item ID-Item Quantity-Item Price]çSalesman name
    public class SaleItem
    {
        public SaleItem(int saleItemId, int quantity, decimal price)
        {
            SaleItemId = saleItemId;
            Quantity = quantity;
            Price = price;
        }

        public int SaleItemId { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
    }
}
