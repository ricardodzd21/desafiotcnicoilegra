﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agibank.Service.Model
{
    public static class FileIn
    {
        public static void Process(ILogger<Worker> logger)
        {
            try
            {
                var homePath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
                homePath = Path.Combine(homePath, "data");                

                if (!Directory.Exists(homePath))
                    Directory.CreateDirectory(homePath);

                var homePathIn = Path.Combine(homePath, "in");
                if (!Directory.Exists(homePathIn))
                    Directory.CreateDirectory(homePathIn);

                var donePath = Path.Combine(homePath, "done");
                if (!Directory.Exists(donePath))
                    Directory.CreateDirectory(donePath);

                var errorPath = Path.Combine(homePath, "error");
                if (!Directory.Exists(errorPath))
                    Directory.CreateDirectory(errorPath);

                var homePathOut = Path.Combine(homePath, "out");
                if (!Directory.Exists(homePathOut))
                    Directory.CreateDirectory(homePathOut);

                logger.LogInformation($"{DateTimeOffset.Now} Read files in folder: {homePathIn}");

                foreach (var directoryFile in Directory.GetFiles(homePathIn))
                {
                    FileOut fileOut = new FileOut();
                    fileOut.FileName = directoryFile.Split("\\").LastOrDefault();
                    try
                    {
                        foreach (var file in File.ReadAllLines(directoryFile))
                        {
                            var dados = file.Split('�');
                            if (dados == null)
                                dados = file.Split('ç');

                            if (dados[0] == "001")
                            {
                                fileOut.Vendors.Add(new Vendor(dados[1], dados[2], decimal.Parse(dados[3])));
                            }
                            else if (dados[0] == "002")
                            {
                                fileOut.Clients.Add(new Client(dados[2], dados[1], dados[3]));
                            }
                            else if (dados[0] == "003")
                            {
                                var vendor = fileOut.Vendors.FirstOrDefault(x => x.Name == dados[3]);
                                Sale sale = new Sale(vendor, int.Parse(dados[1]));
                                vendor.Sales.Add(sale);
                                foreach (var item in dados[2].Replace("]", "").Replace("[", "").Split(','))
                                {
                                    var itens = item.Split('-');
                                    sale.Items.Add(new SaleItem(int.Parse(itens[0]), int.Parse(itens[1]), decimal.Parse(itens[2])));
                                }
                            }
                        }
                        var textReport = $@"
Quantidade de clientes no arquivo de entrada: {fileOut.Clients.Count()}
• Quantidade de vendedores no arquivo de entrada: {fileOut.Vendors.Count()}
• ID da venda mais cara :  {fileOut.IdSaleMajor}
O pior vendedor :  {fileOut.VendorMinor}";
                        File.WriteAllText(Path.Combine(homePathOut, fileOut.FileName), textReport);

                        File.Move(Path.Combine(homePathIn, fileOut.FileName), Path.Combine(donePath, fileOut.FileName), true);
                        logger.LogInformation($"Worker process file: {fileOut.FileName}");
                    }

                    catch
                    {
                        logger.LogError($"Worker process file: {fileOut.FileName}");
                        File.Move(Path.Combine(homePathIn, fileOut.FileName), Path.Combine(errorPath, fileOut.FileName), true);
                    }
                }
            }
            catch(Exception ex)
            {
                logger.LogError($"error: {ex.Message}");
            }
        }
    }
}
