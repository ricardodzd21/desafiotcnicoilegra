﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agibank.Service.Model
{
    public class Client
    {
        public Client(string cNPJ, string name, string businessArea)
        {
            CNPJ = cNPJ;
            Name = name;
            BusinessArea = businessArea;
        }

        public string CNPJ { get; set; }
        public string Name { get; set; }
        public string BusinessArea { get; set; }
    }
}
