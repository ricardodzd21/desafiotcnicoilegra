﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Agibank.Service.Model
{
    public class FileOut
    {
        public FileOut()
        {
            this.Clients = new List<Client>();
            this.Vendors = new List<Vendor>();
        }
        public string FileName { get; set; }
        public List<Client> Clients{ get; set; }
        public List<Vendor> Vendors { get; set; }
       
        public int IdSaleMajor => Vendors.OrderByDescending(x => x.SaleMajor).FirstOrDefault().SaleMajor.IdSaleMajor;
        public string VendorMinor => Vendors.OrderBy(x => x.SaleMinor).FirstOrDefault().Name;
    }
}
