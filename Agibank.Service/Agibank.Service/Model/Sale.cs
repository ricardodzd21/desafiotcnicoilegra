﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Agibank.Service.Model
{
    public class Sale
    {
        public Sale( Vendor vendor, int saleId)
        {
            this.Vendor = vendor;
            this.SaleId = saleId;
            this.Items = new List<SaleItem>();
        }
        public int SaleId { get; set; }
        public Vendor Vendor { get; set; }
        public List<SaleItem> Items { get; set; }
        public decimal ValueSum => Items.Sum(x => x.Price);

    }
}
